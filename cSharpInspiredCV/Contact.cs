﻿using ConsoleTables;
using System.Collections.Generic;

namespace cSharpInspiredCV
{
    public class Contact
    {
        public static void DisplayContactInfo()
        {
            var person = new List<Person>
            {
                new Person
                {
                Name = "Volha",
                Surname = "Hardzei",
                Gender = 'F',
                Phone = "+375295517749",
                Email = "def01fun@gmail.com",
                Location = "Minsk, Belarus",
                Relocate = true
                }
            };
            var table = ConsoleTable.From(person);
            table.Write();
        }
    }
}
