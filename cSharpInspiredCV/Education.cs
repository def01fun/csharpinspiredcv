﻿using ConsoleTables;
using System;

namespace cSharpInspiredCV
{
    public class Education
    {
        public static void DisplayEducation()
        {
            var table = new ConsoleTable("University", "Period", "Degree");
            table.AddRow("Belarusian State University", "2016-2020", "Researcher")
                 .AddRow("Baranovichi State University", "2014-2016", "Master’s degree")
                 .AddRow("Baranovichi State University", "2009-2014", "Bachelor degree");
            table.Write();
            Console.WriteLine();
            Console.WriteLine("Languages: English - C1, Belarusian");
            Console.WriteLine();
        }
    }
}
