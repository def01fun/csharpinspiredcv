﻿using System;
using System.Collections.Generic;

namespace cSharpInspiredCV
{
    public class Questions
    {
        public static void AskQuestions()
        {
            string initialQuestion = "Is your company looking for an experienced and creative Quality Assurance Engineer?\n";
            string education = "Would you like to get education details?\n";
            string work = "Would you like to get work experience details?\n";

            List<string> questions = new List<string> { initialQuestion, education, work};

            ConsoleKeyInfo userInput;
            foreach (var question in questions)
            {
                TextAminationUtils.Type(question);
                Console.WriteLine("Press Y for YES and any other key for NO.");
                userInput = Console.ReadKey();
                Console.WriteLine();

                if (userInput.Key.ToString().Equals("Y") || userInput.Key.ToString().Equals("y"))
                {
                    TextAminationUtils.Type("All right, just give me a sec.\n");
                    string[] frames = { "<", "^", ">", "V" };
                    TextAminationUtils.AnimateFrames(frames);
                    TextAminationUtils.Type("Well, here it is.\n");
                    if (question == initialQuestion)
                        Contact.DisplayContactInfo();
                    else if (question == education)
                        Education.DisplayEducation();
                    else if (question == work)
                        WorkExperience.DisplayExperience();
                }
                else
                {
                    TextAminationUtils.Type("Are you sure?\n");
                    string[] frames = { "(o_o)", "(O_o)", "(o_O)" };
                    TextAminationUtils.AnimateFrames(frames);
                }
            }
            TextAminationUtils.Type("Congratulations! Level 1 completed.\nNext step: contact Volha and ask for an interview.");
        }
    }
}
