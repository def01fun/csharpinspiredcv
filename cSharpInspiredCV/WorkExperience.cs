﻿using ConsoleTables;

namespace cSharpInspiredCV
{
    public class WorkExperience
    {
        public static void DisplayExperience()
        {
            TextAminationUtils.Type("Working at Allied Testing as a contractor for the following projects.\n");
            var table = new ConsoleTable("Project", "Period", "Details");
            table.AddRow("RX Global", "December 2021 – Current", "UI and API Automation, C#")
                 .AddRow("The Economist", "September 2020 – December 2021", "Data analytics, SQL")
                 .AddRow("Refinitiv", "September 2018 – September 2020", "Fintech, Cucumber, SQL");
            table.Write();
        }
    }
}
