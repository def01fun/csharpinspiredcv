﻿namespace cSharpInspiredCV
{
    public class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public char Gender { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Location { get; set; }
        public bool Relocate { get; set; }
    }
}
