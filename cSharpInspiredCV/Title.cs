﻿using System;

namespace cSharpInspiredCV
{
    public class Title
    {
        public static void WriteName()
        {
            Console.Title = "C Sharp-inspired CV";
            string name = @"
    _   __      __  __ 	                                  _ 
   | | / /__   / / / / 	  ____ _    _    _               / /            _ 
   | |/ / __ `/ / / /_   / __ `/   / / / /__  _	__   ___/ /_____  ___  (_)
   |   / /_/ / /_/  _  `/ /_/ /	  / /_/ / __ `/`__`/ _   /___  / / _ \/ / 
   |__/\____/\__/_/  /_/\__,_/	 / _   / /_/ / /  / /_` /   / /_/  __ )/
                                /_/ /_/\__,_/_/   \____/  /____/\___/_/

";
            TextAminationUtils.Blink(name);
        }

        public static void WriteTitle()
        {
            string title = @"
 ____  _  _  ____  ____  _  _  __  ____     __  ____     __   _  _   __   __    __  ____  _  _ 
(  _ \/ )( \(  _ \/ ___)/ )( \(  )(_  _)   /  \(  __)   /  \ / )( \ / _\ (  )  (  )(_  _)( \/ )
 ) __/) \/ ( )   /\___ \) \/ ( )(   )(    (  O )) _)   (  O )) \/ (/    \/ (_/\ )(   )(   )  / 
(__)  \____/(__\_)(____/\____/(__) (__)    \__/(__)     \__\)\____/\_/\_/\____/(__) (__) (__/  
";
            Console.WriteLine(title);
        }
    }
}
