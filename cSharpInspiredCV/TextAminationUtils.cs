﻿using System;
using System.Threading;
using static System.Console;


namespace cSharpInspiredCV
{
    internal class TextAminationUtils
    {
        protected static int origRow;
        protected static int origCol;

        public static void Blink(string text, int blinkCount = 2, int onTime = 800, int offTime = 250)
        {
            CursorVisible = false;
            for (int i = 0; i < blinkCount; i++)
            {
                WriteLine(text);
                Thread.Sleep(onTime);
                Clear();
                Thread.Sleep(offTime);
            }
            WriteLine(text);
            Thread.Sleep(onTime);
            CursorVisible = true;
        }

        public static void Type(string text, int delay = 30)
        {
            char[] textToChar = text.ToCharArray();
            foreach (var item in textToChar)
            {
                Write(item);
                Thread.Sleep(delay);
            }            
        }

        public static void AnimateFrames(string[] frames)
        {            
            origRow = Console.CursorTop;
            origCol = Console.CursorLeft;
            CursorVisible = false;
            for (int i = 0; i < 5; i++)
            {
                foreach (var frame in frames)
                {
                    Console.SetCursorPosition(origCol, origRow);
                    WriteLine(frame);
                    Thread.Sleep(100);
                }
            }
            CursorVisible = true;
        }
    }
}
